/*
 * Mini Computin - musical instrument for ArduBoy.
 * 
 * author:    Miloslav "drummyfish" Ciz
 * year:      2018
 * version:   1.0
 * license:   CC0
 */

#include <Arduboy2.h>

#define DISPLAY_WIDTH 128
#define DISPLAY_HEIGHT 64
#define FRAMERATE 60
#define BEGIN_FREQUENCY 65  // C2, should always be C to not mess up the key colors
#define END_FREQUENCY 261   // C4
#define TREMOLLO_LEVELS 3
#define CURSOR_WIDTH 5
#define CURSOR_SPEED 2

Arduboy2 arduboy;
BeepPin1 beep1;
BeepPin2 beep2;

int8_t background[DISPLAY_WIDTH];  // holds precomputed info about the background
int8_t cursorPos[2];

float linearHelper1;
float linearHelper2;

uint8_t previousTrem;
uint8_t previousFreq;
uint8_t previousStereo;

uint8_t tremolloPeriod;

uint16_t positionToFrequency(uint8_t xPosition)
{
  float ratio = xPosition / 128.0;
  return pow(2,ratio * linearHelper1 + (1.0 - ratio) * linearHelper2);
}

void playTone(uint16_t freq, uint8_t trem, bool stereo)  // giving freq 0 stops the sound
{
  // no change in parameters? => do nothing
  if (trem == previousTrem && freq == previousFreq && stereo == previousStereo && arduboy.frameCount % tremolloPeriod != 0)
    return;

  // handle tremollo (turning the sound on/off with given period)
  if (arduboy.frameCount % tremolloPeriod == 0)
  {
    if (tremolloPeriod == 0)  // 0 => no tremollo
      return;
      
    if ((arduboy.frameCount / tremolloPeriod) % 2 == 0)
      freq = 0;
  }

  previousTrem = trem;
  previousFreq = freq;
  previousStereo = stereo;
  
  tremolloPeriod = TREMOLLO_LEVELS - trem;
  
  if (freq > 0)
  {
    beep1.tone(beep1.freq(freq));

    if (stereo)
      beep2.tone(beep2.freq(freq));
  }
  else
  {
    beep1.noTone();
    beep2.noTone();
  }
}

void setup()
{
  arduboy.begin();
  arduboy.setFrameRate(FRAMERATE);

  arduboy.audio.begin();
  arduboy.audio.on();

  beep1.begin();
  beep2.begin();

  previousTrem = 0;
  previousFreq = 0;
  previousStereo = false;
  
  cursorPos[0] = DISPLAY_WIDTH / 2;
  cursorPos[1] = DISPLAY_HEIGHT / 2;

  linearHelper1 = log(END_FREQUENCY) / log(2);
  linearHelper2 = log(BEGIN_FREQUENCY) / log(2);

  // precompute the background:

  float toneMultiple = pow(2,1.0 / 12.0);
  float nextTone = BEGIN_FREQUENCY * toneMultiple;

  uint8_t toneNumber = 0;

  for (uint8_t i = 0; i < DISPLAY_WIDTH; ++i)
  {
    background[i] = toneNumber == 1 || toneNumber == 3 || toneNumber == 6 || toneNumber == 8 || toneNumber == 10;
    
    if (positionToFrequency(i) >= nextTone)
    {
      background[i] = 3; // dotted line
      nextTone *= toneMultiple;
      toneNumber = (toneNumber + 1) % 12;
    }
  }
}

void loop()
{
  if (!(arduboy.nextFrame()))
    return;

  beep1.timer();
  beep2.timer();

  if (arduboy.pressed(RIGHT_BUTTON))
    cursorPos[0] = min(cursorPos[0] + CURSOR_SPEED,DISPLAY_WIDTH - 1);
    
  if (arduboy.pressed(LEFT_BUTTON))
    cursorPos[0] = max(cursorPos[0] - CURSOR_SPEED,0);

  if (arduboy.pressed(UP_BUTTON))
    cursorPos[1] = max(cursorPos[1] - CURSOR_SPEED,0);

  if (arduboy.pressed(DOWN_BUTTON))
    cursorPos[1] = min(cursorPos[1] + CURSOR_SPEED,DISPLAY_HEIGHT - 1);

  if (arduboy.pressed(A_BUTTON) || arduboy.pressed(B_BUTTON))
    playTone(positionToFrequency(cursorPos[0]),(DISPLAY_HEIGHT - cursorPos[1] - 1) / (DISPLAY_HEIGHT / TREMOLLO_LEVELS),arduboy.pressed(B_BUTTON));
  else
    playTone(0,0,false);   // stop the sound

  arduboy.clear();

  // draw background:
  for (uint8_t i = 0; i < DISPLAY_WIDTH; ++i)
    if (background[i] == 0)
      arduboy.drawFastVLine(i,0,DISPLAY_HEIGHT);
    else if (background[i] == 3)
      for (uint8_t j = 0; j < DISPLAY_HEIGHT; ++j)
        arduboy.drawPixel(i,j,j % 2);

  // draw cursor:
  arduboy.drawFastHLine(cursorPos[0] - CURSOR_WIDTH / 2,cursorPos[1],CURSOR_WIDTH);
  arduboy.drawFastVLine(cursorPos[0],cursorPos[1] - CURSOR_WIDTH / 2,CURSOR_WIDTH);
  arduboy.drawPixel(cursorPos[0],cursorPos[1],BLACK);

  arduboy.display();
}

